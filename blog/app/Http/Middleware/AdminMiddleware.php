<?php

namespace App\Http\Middleware;

use Closure;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();
        if ($user) {
            if ($request->route()->named('super')) {
                if ($user->isSuper()) {
                    return $next($request);
                }
            }elseif ($request->route()->named('admin')) {
                if ($user->isAdmin()) {
                    return $next($request);
                }
            }else {
                
                return $next($request);
            }

            }

        return abort('403');

    }
}
